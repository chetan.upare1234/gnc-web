<?php
if (!defined('ABSPATH')) {
   exit; // Exit if accessed directly.
}


/**
 * GNC Elementor Hero Widget
 *
 * Elementor widget that inserts a hero.
 *
 * @since 1.0.0
 */
class GNC_Elementor_Hero_Widget extends \Elementor\Widget_Base
{


   /**
    * Get Js & css
    */
   public function __construct($data = [], $args = null)
   {
      parent::__construct($data, $args);

      //wp_register_script('script-handle', plugin_dir_url(__FILE__) . 'gnc-slider.js', ['elementor-frontend'], '1.0.0', true);
      //wp_register_style('style-handle', plugin_dir_url(__FILE__) . 'slick.css');
      wp_register_style('style-handle', plugin_dir_url(__FILE__) . 'gnc-hero.css');

      //wp_register_style( 'style-handle', plugin_dir_url( __FILE__ ).'gnc-custom.css');

   }

   public function get_script_depends()
   {
      return ['style-handle'];
   }

   /**
    * Get widget name.
    *
    * Retrieve Hero widget name.
    *
    * @since 1.0.0
    * @access public
    * @return string Widget name.
    */
   public function get_name()
   {
      return 'GNC Hero';
   }


   /**
    * Get widget title.
    *
    * Retrieve Card widget title.
    *
    * @since 1.0.0
    * @access public
    * @return string Widget title.
    */
   public function get_title()
   {
      return esc_html__('GNC Hero', 'gnc-hero-widget');
   }

   /**
    * Get widget icon.
    *
    * Retrieve Card widget icon.
    *
    * @since 1.0.0
    * @access public
    * @return string Widget icon.
    */
   public function get_icon()
   {
      return 'eicon-thumbnails-half';
   }


   /**
    * Get custom help URL.
    *
    * Retrieve a URL where the user can get more information about the widget.
    *
    * @since 1.0.0
    * @access public
    * @return string Widget help URL.
    */
   public function get_custom_help_url()
   {
      return 'https://gnc.com/';
   }

   /**
    * Get widget categories.
    *
    * Retrieve the list of categories the hero widget belongs to.
    *
    * @since 1.0.0
    * @access public
    * @return array Widget categories.
    */
   public function get_categories()
   {
      return ['general'];
   }

   /**
    * Get widget keywords.
    *
    * Retrieve the list of keywords the Hero widget belongs to.
    *
    * @since 1.0.0
    * @access public
    * @return array Widget keywords.
    */
   public function get_keywords()
   {
      return ['top', 'hero', 'heading', 'custom'];
   }



   /**
    * Register Hero widget controls.
    *
    * Add input fields to allow the user to customize the widget settings.
    *
    * @since 1.0.0
    * @access protected
    */
   protected function register_controls()
   {



      // Hero Text 

      $this->start_controls_section(
         'content_section',
         [
            'label' => esc_html__('Text Content', 'gnc-hero-widget'),
            'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
         ]
      );

      $this->add_control(
         'category_title',
         [
            'label' => esc_html__('Title', 'gnc-hero-widget'),
            'type' => \Elementor\Controls_Manager::TEXT,
            'label_block' => true,
            'placeholder' => esc_html__('category title here', 'gnc-hero-widget'),
            'dynamic' => [
               'active' => true,
            ],
         ]
      );


      $this->add_control(
         'category_description',
         [
            'label' => esc_html__('Description', 'gnc-hero-widget'),
            'type' => \Elementor\Controls_Manager::TEXTAREA,
            'label_block'   => true,
            'placeholder' => esc_html__('category description here', 'gnc-hero-widget'),
            'dynamic' => [
               'active' => true,
            ],
         ]
      );

      $this->end_controls_section();


      // Image Section Controls

      $this->start_controls_section(
         'image_section',
         [
            'label' => esc_html__('Image Section', 'gnc-hero-widget'),
            'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
         ]
      );

      $this->add_control(
         'category_bg_d',
         [
            'label' => esc_html__('Choose Desktop Background', 'gnc-hero-widget'),
            'type' => \Elementor\Controls_Manager::MEDIA,
            'default' => [
               'url' => \Elementor\Utils::get_placeholder_image_src(),
            ],
            'dynamic' => [
               'active' => true,
            ],
         ]
      );

      $this->add_control(
         'category_bg_m',
         [
            'label' => esc_html__('Choose Mobile Background', 'gnc-hero-widget'),
            'type' => \Elementor\Controls_Manager::MEDIA,
            'default' => [
               'url' => \Elementor\Utils::get_placeholder_image_src(),
            ],
            'dynamic' => [
               'active' => true,
            ],
         ]
      );
      $this->add_control(
         'category_bg_t',
         [
            'label' => esc_html__('Choose Tablet Background', 'gnc-hero-widget'),
            'type' => \Elementor\Controls_Manager::MEDIA,
            'default' => [
               'url' => \Elementor\Utils::get_placeholder_image_src(),
            ],
            'dynamic' => [
               'active' => true,
            ],
         ]
      );



      $this->end_controls_section();

      //Alignment Section

      $this->start_controls_section(
         'alignment_section',
         [
            'label' => esc_html__('Alignment', 'gnc-hero-widget'),
            'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
         ]
      );

      $this->add_control(
         'category_al',
         [
            'type' => \Elementor\Controls_Manager::CHOOSE,
            'label' => esc_html__('Alignment Of Text', 'plugin-name'),
            'options' => [
               'left' => [
                  'title' => esc_html__('Text Left ', 'gnc-hero-widget'),
                  'icon' => 'eicon-text-align-left',
               ],
               'right' => [
                  'title' => esc_html__('Text Right', 'gnc-hero-widget'),
                  'icon' => 'eicon-text-align-right',
               ],
            ],
            'default' => 'Left',
            'dynamic' => [
               'active' => true,
            ],
         ]
      );

      $this->end_controls_section();
   }

   /**
    * Render Card widget output on the frontend.
    *
    * Written in PHP and used to generate the final HTML.
    *
    * @since 1.0.0
    * @access protected
    */
   protected function render()
   {

 // get our input from the widget settings.
      $settings = $this->get_settings_for_display();

      // Content Values:get the individual values of the input
      $category_title = $settings['category_title'];
      $category_description = $settings['category_description'];
      $category_bg_d = $settings['category_bg_d'];
      $category_bg_t = $settings['category_bg_t'];
      $category_bg_m = $settings['category_bg_m'];
      $cat_text_float = $settings['category_al'];

      $catgtextblock_float='';

      if($cat_text_float=='right'){
         $catgtextblock_float='float-right';
      } 


?>



<div class="gnc-catgsection">
         <div class="gnc-catgpanel">
           
            <?php
            /* 
              set float right text when GNC hero Aignment Set Right By Default It is Left. */
              if($catgtextblock_float=='float-right'){
               
            ?>
             <div class="catgtextblock <?php echo $catgtextblock_float; ?>">
            <?php 
             }else{
             ?>
            <div class="catgtextblock">

            <?php    
             }
            ?>
               <h1 class="header"><?php echo $category_title; ?></h1>
               <p class="subtext"><?php echo $category_description;  ?></p>
            </div>
            <div class="catgimgblock">
               <picture class="pd-image-picture">
                  <source media="(min-width:1024px)" srcset="<?php echo $category_bg_d['url']; ?>">
                  <source media="(min-width:521px)" srcset="<?php echo $category_bg_t['url']; ?>">
                  <img class="heroslide-tile-img" data-src="<?php echo $category_bg_m['url']; ?>" alt="image" src="<?php echo $category_bg_m['url']; ?>">
               </picture>
            </div>
         </div>
      </div>


      <!-- End rendering the output -->

<?php


   }
}
