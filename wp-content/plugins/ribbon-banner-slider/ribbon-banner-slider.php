<?php
/**
 * Plugin Name: Ribbon Banner SLider
 * Description: This plugin will slide only text
 * Version: 1.0
 *
 */

add_action('wp_enqueue_scripts','ribbon_banner_init');

function ribbon_banner_init() {    
    wp_enqueue_script( 'default', plugins_url( '/js/jquery.min.js', __FILE__ ));  
    wp_enqueue_script( 'slick-min-js', plugins_url( '/js/slick.min.js', __FILE__ )); 
    wp_enqueue_script( 'custom-js', plugins_url( '/js/custom.js', __FILE__ ));  
    wp_enqueue_style( 'slick-carousel', plugins_url( '/css/slick.css', __FILE__ ));
    wp_enqueue_style( 'slick-default', plugins_url( '/css/slick-theme.min.css', __FILE__ ));
    wp_enqueue_style( 'slick-default', plugins_url( '/css/custom.css', __FILE__ ));
	wp_register_style( 'custom_css', plugin_dir_url( __FILE__ ) . 'css/custom.css', false, '1.0.0' );
}

add_shortcode('ribbon-banner-slider','ribbon_banner_slider');

function ribbon_banner_slider(){

    $args = array(
        'post_type' => 'ribbon_banner',
        'posts_per_page' => 5
        // Several more arguments could go here. Last one without a comma.
    );

// Query the posts:
$silder_query = new WP_Query($args); ?>
 <div class="buttons">
<button id='toggle' class="gnc-play">pause</button>

</div>
<section class="slider">
    <?php 
    while ($silder_query->have_posts()) : $silder_query->the_post(); $ribbon_banner_item_url = get_field('ribbon_banner_item_url', get_the_ID());
        echo '<div class="item">';
        echo '<a href="'.$ribbon_banner_item_url.'" class="ribbon_banner_item">'.get_the_title().'</a>';
        echo '</div>';
    endwhile;
    wp_reset_postdata();
    ?>
    </section>

<?php   }  ?>


