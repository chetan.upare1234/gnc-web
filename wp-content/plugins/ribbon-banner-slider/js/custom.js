$( document ).ready(function() {
	$(".slider").slick({
        dots: false,
        fade: false,
        pauseOnHover: false,
        arrows: true,
        infinite: true,
        autoplay: true,
        autoplaySpeed: 1000,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true
});

$('#toggle').click( function() {
  if ($(this).hasClass('gnc-play')){
     $('.slider').slick('slickPause')
     $(this).html('play') 
	 $(this).removeClass('gnc-play');
	 $(this).addClass('gnc-pause');
  } else {
    $('.slider').slick('slickPlay')  
    $(this).html('pause') 
	$(this).removeClass('gnc-pause');
	$(this).addClass('gnc-play');	
  }  
});
});