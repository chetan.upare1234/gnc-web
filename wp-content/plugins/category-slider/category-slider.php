<?php
/**
 * Plugin Name: Category & Tags Slider
 * Plugin URI: https://www.gnc.com/
 * Description: Display Article's Category & Tag Slider To Elementor Frontend
 * Version: 0.1
 * Text Domain: category-slider
 * Author: --
 * Author URI: https://gnc.com
 */



// Tag Slider Shortcode [tag-slider]

add_shortcode('tag-slider', 'category_page_tag_filter_section_callback');
function category_page_tag_filter_section_callback() {
    ob_start();
  $data=array();
  ?>


   <div class="search-tgspanel">
         <div class="search-tagsslide">
            <div class="search-tagsslidewrapper">
                 <?php
 
                 $arr_tag_list = get_tags(array(
                   'hide_empty' => false
                 ));
 
                  foreach($arr_tag_list as $single_tag){
 
                  ?>
                    <span>
                      <a href="<?php echo esc_url( $single_tag->term_id ); ?>" class="active"><?php echo $single_tag->name; ?></a>
                    </span>
                  <?php
 
                  }
                  ?>
 
                 <?php
 
                 ?>
            </div>
           </div>
           
        </div>
         <?php return ob_get_clean(); 
 
                }
 
            
//Category Slider Shortcode [cat-slider]

add_shortcode('cat-slider', 'custom_home_page_get_category');
function custom_home_page_get_category(){

    ob_start();
    $terms = get_terms( array(
    'taxonomy' => 'category',
    'hide_empty' => false,
) );

 ?>
 

   <head>
    

   </head>

    <div class="category-slider">
      <div class="categoriesslidercontainer">
         <div class="categoriessliderwrapper">
            <h2>Categories</h2>
            <div class="categoriescarousel">

                <?php
    foreach($terms as $single_term){

        $taxonomy = $single_term->taxonomy;
        $term_id = $single_term->term_id;
        $image = get_field('image', $taxonomy . '_' . $term_id);
        $category_link = get_category_link($single_term->term_id);

                ?>
               <span>
                  <div class="categoriescomponent">
                     <div class="categoriesinner">
                        <div class="media-wrap">
                            <picture class="pd-image-picture">
                                <img class="categoriess-tile-img" data-src="<?php if(isset($image['url'])){ echo $image['url'];} ?>" alt="wellness" style=" " ie-style=" " src="<?php if(isset($image['url'])){ echo $image['url'];} ?>">
                            </picture>
                        </div>
                        <div class="cardinfo-wrap">
                           <div class="card-text-wrap">
                              <h4 class="card-title" ><?php echo $single_term->name; ?></h4>
                              <p class="card-subtitle"><?php echo $single_term->description; ?></p>
                              <div class="card-btn"><a class="card-link" href="<?php echo esc_url( $category_link ); ?>" target="" >View Articles</a></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </span>
               <?php
                    }

                ?>

            </div>
            <div class="categorie-rangeslider"></div>
         </div>
      </div>
        <?php
        return ob_get_clean();


} ?>
