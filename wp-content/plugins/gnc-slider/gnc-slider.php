<?php
/**
 * Plugin Name: GNC Slider Elementor Widget
 * Description: Elementor custom widgets for GNC Slider.
 * Plugin URI:  https://gnc.com/
 * Version:     1.0.0
 * Author:      GNC Devs
 * Author URI:  https://gnc.com/
 * Text Domain: gnc-slider
 *
 * Elementor tested up to: 3.5.0
 * Elementor Pro tested up to: 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Security : Exit if accessed directly.
}

function register_gnc_slider_widgets( $widgets_manager ) {

    require_once( __DIR__ . '/widgets/slider-widget.php' );  // include the widget file

    $widgets_manager->register( new \GNC_Elementor_Slider_Widget() );  // register the widget

}
add_action( 'elementor/widgets/register', 'register_gnc_slider_widgets' );

