<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/**
 * GNC Elementor Slider Widget.
 *
 * Elementor widget that inserts a slider.
 *
 * @since 1.0.0
 */
class GNC_Elementor_Slider_Widget extends \Elementor\Widget_Base {


    /**
     * Get Js & css
     */
    public function __construct($data = [], $args = null) {
      parent::__construct($data, $args);

      wp_register_script( 'script-handle', plugin_dir_url( __FILE__ ).'gnc-slider.js', [ 'elementor-frontend' ], '1.0.0', true );
      wp_register_style( 'style-handle', plugin_dir_url( __FILE__ ).'slick.css');
      wp_register_style( 'style-handle', plugin_dir_url( __FILE__ ).'gnc.css');

      //wp_register_style( 'style-handle', plugin_dir_url( __FILE__ ).'gnc-custom.css');

   }

   public function get_script_depends() {
       return [ 'script-handle' ];
   }

  	/**
	 * Get widget name.
	 *
	 * Retrieve Slider widget name.
	 *
	 * @since 1.0.0
	 * @access public
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'GNC Slider';
	}


	/**
	 * Get widget title.
	 *
	 * Retrieve Card widget title.
	 *
	 * @since 1.0.0
	 * @access public
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'GNC Slider', 'gnc-slider-widget' );
	}

	/**
	 * Get widget icon.
	 *
	 * Retrieve Card widget icon.
	 *
	 * @since 1.0.0
	 * @access public
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-slider-3d';
	}


	/**
	 * Get custom help URL.
	 *
	 * Retrieve a URL where the user can get more information about the widget.
	 *
	 * @since 1.0.0
	 * @access public
	 * @return string Widget help URL.
	 */
	public function get_custom_help_url() {
		return 'https://gnc.com/';
	}

	/**
	 * Get widget categories.
	 *
	 * Retrieve the list of categories the Slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'general' ];
	}

	/**
	 * Get widget keywords.
	 *
	 * Retrieve the list of keywords the Slider widget belongs to.
	 *
	 * @since 1.0.0
	 * @access public
	 * @return array Widget keywords.
	 */
	public function get_keywords() {
		return [ 'slider', 'hero', 'corausal', 'custom' ];
	}



	/**
	 * Register Slider widget controls.
	 *
	 * Add input fields to allow the user to customize the widget settings.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function register_controls() {



		// Slider 1

		$this->start_controls_section(
			'slider1_section',
			[
				'label' => esc_html__( 'Slider 1', 'gnc-slider-widget' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'slider1_title',
			[
				'label' => esc_html__( 'title', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Your title here', 'gnc-slider-widget' ),
            'dynamic' => [
               'active' => true,
            ],
			]
		);


		$this->add_control(
			'slider1_description',
			[
				'label' => esc_html__( 'Description', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block'   => true,
				'placeholder' => esc_html__( 'Your description here', 'gnc-slider-widget' ),
            'dynamic' => [
               'active' => true,
            ],
			]
		);

        $this->add_control(
			'slider1_btn_txt',
			[
				'label' => esc_html__( 'Button Text', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block'   => false,
				'placeholder' => esc_html__( 'Click Here', 'gnc-slider-widget' ),
            'dynamic' => [
               'active' => true,
            ],
			]
		);

        $this->add_control(
			'slider1_btn_url',
			[
				'label' => esc_html__( 'Link', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => esc_html__( 'https://', 'gnc-slider-widget' ),
				'options' => [ 'url', 'is_external', 'nofollow' ],
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
					// 'custom_attributes' => '',
				],
				'label_block' => true,
			]
		);

      $this->add_control(
			'slider1_al',
			[
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'label' => esc_html__( 'Alignment', 'plugin-name' ),
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'gnc-slider-widget' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'gnc-slider-widget' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'gnc-slider-widget' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'default' => 'left',
            'dynamic' => [
               'active' => true,
            ],
			]
         );

        $this->add_control(
			'slider1_bg_d',
			[
				'label' => esc_html__( 'Choose Desktop Background', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
            'dynamic' => [
               'active' => true,
            ],
			]
		);

        $this->add_control(
			'slider1_bg_m',
			[
				'label' => esc_html__( 'Choose Mobile Background', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
            'dynamic' => [
               'active' => true,
            ],
			]
		);
        $this->add_control(
			'slider1_bg_t',
			[
				'label' => esc_html__( 'Choose Tablet Background', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
            'dynamic' => [
               'active' => true,
            ],
			]
		);

        $this->end_controls_section();

        // Slider 2

        $this->start_controls_section(
			'slider2_section',
			[
				'label' => esc_html__( 'Slider 2', 'gnc-slider-widget' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'slider2_title',
			[
				'label' => esc_html__( 'title', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Your title here', 'gnc-slider-widget' ),
            'dynamic' => [
               'active' => true,
            ],
			]
		);


		$this->add_control(
			'slider2_description',
			[
				'label' => esc_html__( 'Description', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block'   => true,
				'placeholder' => esc_html__( 'Your description here', 'gnc-slider-widget' ),
            'dynamic' => [
               'active' => true,
            ],
			]
		);

        $this->add_control(
			'slider2_btn_txt',
			[
				'label' => esc_html__( 'Button Text', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block'   => false,
				'placeholder' => esc_html__( 'Click Here', 'gnc-slider-widget' ),
            'dynamic' => [
               'active' => true,
            ],
			]
		);

        $this->add_control(
			'slider2_btn_url',
			[
				'label' => esc_html__( 'Link', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => esc_html__( 'https://', 'gnc-slider-widget' ),
				'options' => [ 'url', 'is_external', 'nofollow' ],
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
					// 'custom_attributes' => '',
				],
				'label_block' => true,
            'dynamic' => [
               'active' => true,
            ],
			]
		);

      $this->add_control(
			'slider2_al',
			[
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'label' => esc_html__( 'Alignment', 'gnc-slider-widget' ),
				'options' => [
					'left' => [
						'title' => esc_html__( 'left', 'gnc-slider-widget' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'gnc-slider-widget' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'gnc-slider-widget' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'default' => 'center',
            'dynamic' => [
               'active' => true,
            ],
			]
         );

        $this->add_control(
			'slider2_bg_d',
			[
				'label' => esc_html__( 'Choose Desktop Background', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
            'dynamic' => [
               'active' => true,
            ],
			]
		);

        $this->add_control(
			'slider2_bg_m',
			[
				'label' => esc_html__( 'Choose Mobile Background', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
            'dynamic' => [
               'active' => true,
            ],
			]
		);
        $this->add_control(
			'slider2_bg_t',
			[
				'label' => esc_html__( 'Choose Tablet Background', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
            'dynamic' => [
               'active' => true,
            ],
			]
		);



		$this->end_controls_section();

        //Slider 3

        $this->start_controls_section(
			'slider3_section',
			[
				'label' => esc_html__( 'Slider 3', 'gnc-slider-widget' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'slider3_title',
			[
				'label' => esc_html__( 'title', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Your title here', 'gnc-slider-widget' ),
            'dynamic' => [
               'active' => true,
            ],
			]
		);


		$this->add_control(
			'slider3_description',
			[
				'label' => esc_html__( 'Description', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'label_block'   => true,
				'placeholder' => esc_html__( 'Your description here', 'gnc-slider-widget' ),
            'dynamic' => [
               'active' => true,
            ],
			]
		);

        $this->add_control(
			'slider3_btn_txt',
			[
				'label' => esc_html__( 'Button Text', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'label_block'   => false,
				'placeholder' => esc_html__( 'Click Here', 'gnc-slider-widget' ),
            'dynamic' => [
               'active' => true,
            ],
			]
		);

        $this->add_control(
			'slider3_btn_url',
			[
				'label' => esc_html__( 'Link', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => esc_html__( 'https://', 'gnc-slider-widget' ),
				'options' => [ 'url', 'is_external', 'nofollow' ],
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
					// 'custom_attributes' => '',
				],
				'label_block' => true,
            'dynamic' => [
               'active' => true,
            ],
			]
		);

      $this->add_control(
			'slider3_al',
			[
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'label' => esc_html__( 'Alignment', 'plugin-name' ),
				'options' => [
					'left' => [
						'title' => esc_html__( 'Left', 'gnc-slider-widget' ),
						'icon' => 'eicon-text-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'gnc-slider-widget' ),
						'icon' => 'eicon-text-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'gnc-slider-widget' ),
						'icon' => 'eicon-text-align-right',
					],
				],
				'default' => 'center',
            'dynamic' => [
               'active' => true,
            ],
			]
         );

        $this->add_control(
			'slider3_bg_d',
			[
				'label' => esc_html__( 'Choose Desktop Background', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
            'dynamic' => [
               'active' => true,
            ],
			]
		);

        $this->add_control(
			'slider3_bg_m',
			[
				'label' => esc_html__( 'Choose Mobile Background', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
            'dynamic' => [
               'active' => true,
            ],
			]
		);
        $this->add_control(
			'slider3_bg_t',
			[
				'label' => esc_html__( 'Choose Tablet Background', 'gnc-slider-widget' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
            'dynamic' => [
               'active' => true,
            ],
			]
		);



		$this->end_controls_section();

	}

	/**
	 * Render Card widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 *
	 * @since 1.0.0
	 * @access protected
	 */
	protected function render() {



		// get our input from the widget settings.
		$settings = $this->get_settings_for_display();

		// Slider 1 :get the individual values of the input
		$slider1_title = $settings['slider1_title'];
		$slider1_description = $settings['slider1_description'];
        $slider1_btn_txt = $settings['slider1_btn_txt'];
        $slider1_btn_url = $settings['slider1_btn_url'];
        $slider1_bg_d = $settings['slider1_bg_d'];
        $slider1_bg_m = $settings['slider1_bg_m'];
        $slider1_bg_t = $settings['slider1_bg_t'];
        $slider1_al = $settings['slider1_al'];
        // Slider 2 :get the individual values of the input
		$slider2_title = $settings['slider2_title'];
		$slider2_description = $settings['slider2_description'];
        $slider2_btn_txt = $settings['slider2_btn_txt'];
        $slider2_btn_url = $settings['slider2_btn_url'];
        $slider2_bg_d = $settings['slider2_bg_d'];
        $slider2_bg_m = $settings['slider2_bg_m'];
        $slider2_bg_t = $settings['slider2_bg_t'];
        $slider2_al = $settings['slider2_al'];
        // Slider 3 :get the individual values of the input
		$slider3_title = $settings['slider3_title'];
		$slider3_description = $settings['slider3_description'];
        $slider3_btn_txt = $settings['slider3_btn_txt'];
        $slider3_btn_url = $settings['slider3_btn_url'];
        $slider3_bg_d = $settings['slider3_bg_d'];
        $slider3_bg_m = $settings['slider3_bg_m'];
        $slider3_bg_t = $settings['slider3_bg_t'];
        $slider3_al = $settings['slider3_al'];

		?>



        <!-- Start rendering the output -->

        <style>
         .containerheroslider.right{
    justify-content: right;
         }
         .containerheroslider .left{
    justify-content: right;
         }
         .containerheroslider center{
    justify-content: center;
         }
        .containerheroslider {
         max-width: 1440px;
         margin-left: auto;
         margin-right: auto;
         height: 700px;
         width: 100%;
         display: flex;
         align-items: center;
         position: relative;
         }
         .hero-subtitle {
         font-family: "proxima-nova", sans-serif;
         font-style: normal;
         font-weight: 400;
         font-size: 16px;
         line-height: 28px;
         color: #fff;
         position: relative;
         }
         .heroimageslider .heromedia-wrap img {
         height: 100%;
         object-fit: cover;
         width: 100%;
         }
         .heroimageslider .heromedia-wrap  {
         width: 100%;
         object-fit: cover;
         }
         .heroimageslider.slick-initialized .heroitem-slide {
         position: relative;
         display: -webkit-flex !important;
         display: flex !important;
         -webkit-align-items: center;
         align-items: flex-end;
         }
         .heroimageslider .heromedia-wrap {
         position: absolute;
         left: 0;
         top: 0;
         height: 100%;
         width: 100%;
         }
         .hslidercontent h1 {
         font-size: 100px;
         line-height: 85px;
         margin-bottom: 15px;
         color: #fff;
         font-weight: 700;
         position: relative;
         text-transform: uppercase;
         }
         .hslidercontent p {
         font-size: 20px;
         line-height: 32px;
         max-width: 600px;
         color: rgba(255, 255, 255, 0.9);
         letter-spacing: 0.01em;
         }
         img {
         border: 0;
         vertical-align: top;
         max-width: 100%;
         height: auto;
         }
         .hslidercontent .cta-link {
         position: relative;
         display: inline-block;
         background: #fff;
         color: #1A1A1A;
         text-align: center;
         line-height: 44px;
         cursor: pointer;
         text-transform: uppercase;
         width: 263px;
         height: 44px;
         border: 1px solid #FFFFFF;
         font-family: "proxima-nova", sans-serif;
         font-style: normal;
         font-weight: 700;
         font-size: 14px;
         text-decoration: none!important;
         }
         .hslidercontent .cta-link a {
         text-decoration: none;
         }
         .hslidercontent .cta-link:hover {
         color: #fff;
         background-color: #1A1A1A;
         text-decoration: none;
         }
         .herobannerslickbtn {
         position: absolute;
         right: 0px;
         bottom: 55px;
         }
         .herobannerslickbtn li {
         display: inline-block;
         }
         .herobannerslickbtn li.prev.slick-arrow {
         right: 30px;
         }
         .heroimageslider .slick-arrow {
         position: relative;
         font-size: 0;
         z-index: 12;
         cursor: pointer;
         border-radius: 100%;
         background: #fff;
         border: 1px solid #000;
         border-radius: 100%;
         width: 40px;
         height: 40px;
         opacity: .9;
         transform: none;
         }
         .heroimageslider .slick-arrow:before {
         content: "";
         display: inline-block;
         border-style: solid;
         border-width: 1px 1px 0 0;
         border-color: #000;
         width: 0.625rem;
         height: 0.625rem;
         transform: rotate(-135deg);
         margin-top: 14px;
         margin-left: 17px;
         }
         .heroimageslider .next.slick-arrow:before {
         transform: rotate(45deg);
         margin-left: 14px;
         }
         .herobannerslickbtn li.prev.slick-arrow:hover,
         .herobannerslickbtn li.next.slick-arrow:hover {
         opacity: 1;
         }
         .heroimageslider .slick-slide .heromedia-wrap {
            transform: scale(1);
            transition: transform 1s;
         }
         .heroimageslider .slick-slide.slick-active .heromedia-wrap {
         transform: scale(1.05);
         }
         .heroimageslider .slick-slide.slick-slide--visible {
         visibility: visible;
         }
         .heroimageslider .slick-slider .slick-list, .heroimageslider .slick-slider .slick-track {
         height: 100%;
         }
         .heroimageslider .slick-slide.slick-active {
         visibility: visible;
         animation: b 1s;
         }
         .heroimageslider .slick-slide {
         opacity: 1!important;
         }
         .heroimageslider .slick-slider .slick-list, .heroimageslider .slick-slider .slick-track {
         transform: translateZ(0);
         width: 100%;
        }
         @keyframes b {
         0% {
         clip-path: polygon(120% 0, 120% 0, 100% 100%, 100% 100%);
         }
         to {
         clip-path: polygon(0 0, 110% 0, 100% 100%, -20% 100%);
         }
         }
         @media (min-width: 1024px) {
            .heroimageslider .hslidercontent {
               max-width: 422px;
            }
            .hslidercontent .cta-btn {
            padding-top: 35px;
         }
         }
         @media (min-width: 1024px) and (max-width: 1465px) {
         .containerheroslider {
         margin-left: 50px;
         margin-right: 50px;
         }
         }
         @media (min-width: 1024px) and (max-width: 1089px) {
         .containerheroslider {
         height: 500px;
         }
         .hslidercontent h1 {
         font-size: 60px;
         line-height: 60px;
         }
         }
         @media (min-width: 1090px) and (max-width: 1129px) {
         .containerheroslider {
         height: 530px;
         }
         .hslidercontent h1 {
         font-size: 60px;
         line-height: 60px;
         }
         }
         @media (min-width: 1130px) and (max-width: 1199px) {
         .containerheroslider {
         height: 576px;
         }
         }
         @media (min-width: 1200px) and (max-width: 1480px) {
         .containerheroslider {
         height: 576px;
         }
         }
         @media (max-width: 1023px) {
         .hslidercontent h1{
         font-size: 48px;
         font-weight: 700;
         line-height: 41px;
         letter-spacing: 0.800000011920929px;
         text-align: center;
         margin-bottom: 5px;
         }
         .hslidercontent h1 br{display:none;}
         .hslidercontent {
         width: 100%;
         max-width: 100%;
         text-align: center;
         padding-bottom: 50px;
         margin-left: 10px;
         margin-right: 10px;
         }
         .hslidercontent .cta-btn {
            padding-top: 30px;
         }
         .containerheroslider {
         align-items: flex-end;
         }
         .heroimageslider ul.slick-dots {
         position: absolute;
         bottom: 10px;
         list-style: none;
         display: block;
         text-align: center;
         padding: 0;
         margin: 0;
         width: 100%;
         }
         .heroimageslider .slick-dots li {
         position: relative;
         display: inline-block;
         height: 1.375rem;
         width: 1.375rem;
         margin: 0 0.3125rem;
         padding: 0;
         cursor: pointer;
         }
         .heroimageslider .slick-dots li button {
          border: 0;
          background: transparent;
          display: block;
          outline: none;
          line-height: 0;
          font-size: 0;
          color: transparent;
          padding: 0px;
          cursor: pointer;
          width: 10px;
         height: 10px;
         border: 1px solid #fff;
         border-radius: 50%;
         box-shadow: 0 0 0 1.5px #000;
         background-color: transparent;
         opacity: 1;
         }
         .heroimageslider .slick-dots li.slick-active button {

            background-color: #fff;
         }
         }
         @media (max-width: 520px) {
         .heroimageslider .heromedia-wrap img,
         .heroimageslider.slick-initialized .heroitem-slide {
         height: 650px;
         }
         }

         </style>



        <div class="heroimageslider">
         <!-- hero slide start -->
         <div class="heroitem-slide">
            <div class="heromedia-wrap">
               <picture class="pd-image-picture">
                  <source media="(min-width:1024px)" srcset="<?php echo $slider1_bg_d['url']; ?>">
                  <source media="(min-width:521px)" srcset="<?php echo $slider1_bg_t['url']; ?>">
                  <img class="heroslide-tile-img" data-src="<?php echo $slider1_bg_m['url']; ?>" alt="image" src="<?php echo $slider1_bg_m['url']; ?>">
               </picture>
            </div>


            <div class="containerheroslider <?php echo $slider1_al; ?>">
               <div class="hslidercontent ">
                  <h1 class="title"><?php echo $slider1_title; ?></h1>
                  <div class="hero-subtitle"><?php echo $slider1_description;  ?></div>
                  <div class="cta-btn">
                     <a href="<?php echo $slider1_btn_url['url'];  ?>" class="cta-link"><?php echo $slider1_btn_txt;  ?></a>
                  </div>
                  <ul class="herobannerslickbtn">
                     <li class="prev"></li>
                     <li class="next"></li>
                  </ul>
               </div>
            </div>
         </div>
         <!-- hero slide end -->
         <!-- hero slide start -->
         <div class="heroitem-slide">
            <div class="heromedia-wrap">
               <picture class="pd-image-picture">
                  <source media="(min-width:1024px)" srcset="<?php echo $slider2_bg_d['url']; ?>">
                  <source media="(min-width:521px)" srcset="<?php echo $slider2_bg_t['url']; ?>">
                  <img class="heroslide-tile-img" data-src="<?php echo $slider2_bg_m['url']; ?>" alt="image" src="<?php echo $slider2_bg_m['url']; ?>">
               </picture>
            </div>
            <div class="containerheroslider <?php echo $slider1_al; ?>">
               <div class="hslidercontent">
                  <h1 class="title"><?php echo $slider2_title;  ?></h1>
                  <div class="hero-subtitle"><?php echo $slider2_description;  ?></div>
                  <div class="cta-btn">
                     <a href="<?php echo $slider2_btn_url['url'];  ?>" class="cta-link"><?php echo $slider2_btn_txt;  ?></a>
                  </div>
                  <ul class="herobannerslickbtn">
                     <li class="prev"></li>
                     <li class="next"></li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="heroitem-slide">
            <div class="heromedia-wrap">
               <picture class="pd-image-picture">
                  <source media="(min-width:1024px)" srcset="<?php echo $slider3_bg_d['url'];  ?>">
                  <source media="(min-width:521px)" srcset="<?php echo $slider3_bg_t['url'];  ?>">
                  <img class="heroslide-tile-img" data-src="<?php echo $slider3_bg_m['url'];  ?>" alt="image" src="<?php echo $slider3_bg_m['url'];  ?>">
               </picture>
            </div>
            <div class="containerheroslider <?php echo $slider1_al; ?>">
               <div class="hslidercontent">
                  <h1 class="title"><?php echo $slider3_title;  ?></h1>
                  <div class="hero-subtitle"><?php echo $slider3_description;  ?></div>
                  <div class="cta-btn">
                     <a href="<?php echo $slider3_btn_url['url'];  ?>" class="cta-link"><?php echo $slider3_btn_txt;  ?></a>
                  </div>
                  <ul class="herobannerslickbtn">
                     <li class="prev"></li>
                     <li class="next"></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
        <!-- End rendering the output -->

        <?php


	}


}
