$(document).ready(function(){
    $('.heroimageslider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      initialSlide: 0,
      speed: 1e3,
      fade: !0,
      arrows: true,
      dots: false,
      swipe: true,
      autoplay: true,
      adaptiveHeight: false,
      prevArrow: $('.prev'),
      nextArrow: $('.next'),
      responsive: [
          {
              breakpoint: 1023,
              settings: {
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  autoplaySpeed: 4000,
                  swipe: true,
                  arrows: false,
                  dots: false,
              }
          }
      ]
    });

  });