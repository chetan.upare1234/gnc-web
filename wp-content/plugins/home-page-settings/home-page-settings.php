<?php
/**
 * Plugin Name: Home Page Settings
 * Plugin URI: https://www.gnc.com
 * Description: Home Page Settings Frontend
 * Version: 0.1
 * Text Domain: category-slider
 * Author: --
 * Author URI: https://gnc.com
 */
define('GNC_HOME_PAGE_PAGE_ID',8);
add_shortcode('home-solution-center-section', 'gnc_home_solution_center_setting_section_shortcode_callbcak');
function gnc_home_solution_center_setting_section_shortcode_callbcak(){
   $data['home_solution_center_setting_repeater'] = get_post_meta(GNC_HOME_PAGE_PAGE_ID, 'home_solution_center_setting_repeater', true);

$data['solution_center_button_design']=get_field('solution_center');
 $button_design='getsupport-link-black';
  if($data['solution_center_button_design']=='white_background'){
      
      $button_design='getsupport-link-white';
  }

 ?>
  
      <div class="solutioncenter-container">
      <div class="solutioncenter-wrapper">
         <div class="solutioncenter-wrap">
            <div class="scslickbtn">
               <div class="slick-slider-dots"></div>
            </div>
         </div>

         <div class="solutioncenter-component">
            <div class="solutioncentercarousel">
              
        <?php 

        
        if(isset($data['home_solution_center_setting_repeater'] ) && !empty($data['home_solution_center_setting_repeater'] )){

        foreach($data['home_solution_center_setting_repeater'] as $single_home_solution_center_setting){
        ?>



               <span title="<?php echo $single_home_solution_center_setting['solution_center_settings_title']; ?>">
                  <div class="solutioncenter-inner">
                     <div class="scmedia-wrap">
                        <div class="scmediacontent">
                           <picture class="pd-image-picture">
                              <img class="feature-tile-img" data-src="i<?php echo $single_home_solution_center_setting['solution_center_settings_image']; ?>" alt="solutioncenter" src="<?php echo $single_home_solution_center_setting['solution_center_settings_image']; ?>">
                           </picture>
                        </div>
                     </div>
                     <div class="cardinfo-wrap">
                        <div class="facard-text-wrap">
                           <div class="getsupport-btn">


                              <a class="getsupport-link <?php echo $button_design; ?>" href="<?php echo $single_home_solution_center_setting['solution_center_settings_button_url']; ?>" target="" ><?php echo $single_home_solution_center_setting['solution_center_settings_button_title']; ?></a>

                           </div>
                        </div>
                     </div>
                  </div>
               </span>
       <?php 
          } 
       }        
       ?>
              
            </div>
            </div>
         </div>
      </div>
   

<?php 
}

add_action('wp_footer', 'adding_custom_style_for_solution_center');
function adding_custom_style_for_solution_center(){

if (is_front_page()){ 
?>
   <script>
   $('.solutioncentercarousel').slick({
         slidesToShow: 1,
         slidesToScroll: 1,
         arrows: false,
         infinite: false,
         variableWidth: true,
         dots: true,
         appendDots: $('.slick-slider-dots'),
                     customPaging: function(slider, i) { 
                  return '<button class="tab">' + $(slider.$slides[i]).attr('title');
                  },
         swipe: true,
         autoplay: false,
         responsive: [
               {
              breakpoint:992,
              settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              variableWidth: false,
              dots: true,
               }
         }
      ]
   });
   </script>
   <?php 
   }
}

add_action( 'cmb2_admin_init', 'gnc_home_solution_center_setting_section_callbcak' );
function gnc_home_solution_center_setting_section_callbcak(){

 /**
     * Sample metabox to demonstrate each field type included
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'gnc_home_solution_center_setting_section',
        'title'         => esc_html__( 'Home Page Solution Center Setting Section', 'cmb2' ),
        'object_types'  => array( 'page' ), // Post type
        'show_on'      => array( 'key' => 'id', 'value' => array(GNC_HOME_PAGE_PAGE_ID) ),

        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
        // 'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.

        /*
         * The following parameter is any additional arguments passed as $callback_args
         * to add_meta_box, if/when applicable.
         *
         * CMB2 does not use these arguments in the add_meta_box callback, however, these args
         * are parsed for certain special properties, like determining Gutenberg/block-editor
         * compatibility.
         *
         * Examples:
         *
         * - Make sure default editor is used as metabox is not compatible with block editor
         *      [ '__block_editor_compatible_meta_box' => false/true ]
         *
         * - Or declare this box exists for backwards compatibility
         *      [ '__back_compat_meta_box' => false ]
         *
         * More: https://wordpress.org/gutenberg/handbook/extensibility/meta-box/
         */
        // 'mb_callback_args' => array( '__block_editor_compatible_meta_box' => false ),
    ) );

    
      
    
        $contact_us_top_section_card_block_repeater = $cmb->add_field( array(
        'id'          => 'home_solution_center_setting_repeater',
        'type'        => 'group',
        'repeatable'  => true,
        'options'     => array(
            'group_title'   => 'Card Block Repeater {#}',
            'add_button'    => 'Add Another Post',
            'remove_button' => 'Remove Post',
            'closed'        => true,  // Repeater fields closed by default - neat & compact.
            'sortable'      => true,  // Allow changing the order of repeated groups.
        ),
    ) );
            $cmb->add_group_field( $contact_us_top_section_card_block_repeater, array(
                'name' => 'Solution Center Title',
                'desc' => 'Solution Center Title',
                'id'   => 'solution_center_settings_title',
                'type' => 'text',
            ) );
                $cmb->add_group_field( $contact_us_top_section_card_block_repeater, array(
                'name'    => 'Solution Center Image',
                'desc'    => 'Solution Center Image',
                'id'      => 'solution_center_settings_image',
                'type'    => 'file',
                //'after_row' => '<h3>Add Content For Right Aligned Box</h3>',
                //'closed'        => false, 
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' => 'Add Background' // Change upload button text. Default: "Add or Upload File"
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    //'type' => 'application/pdf', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    // 'type' => array(
                    //     'image/gif',
                    //     'image/jpeg',
                    //     'image/png',
                    // ),
                ),
                'preview_size' => 'small', // Image size to use when previewing in the admin.
            ) );

             $cmb->add_group_field( $contact_us_top_section_card_block_repeater, array(
                'name' => 'Solution Center Button Title',
                'desc' => 'Solution Center Button Title',
                'id'   => 'solution_center_settings_button_title',
                'type' => 'text',
            ) );    

            $cmb->add_group_field( $contact_us_top_section_card_block_repeater, array(
                'name' => 'Solution Center Button URL',
                'desc' => 'Solution Center Button UR',
                'id'   => 'solution_center_settings_button_url',
                'type' => 'text',
            ) );    

    
}