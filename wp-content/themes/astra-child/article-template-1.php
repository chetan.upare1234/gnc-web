<?php
/* 
* Template Name: Articles Template 1
* Template Post Type: Post
*/
get_header();
?>
<?php 
if (have_posts()): 
while (have_posts()) : the_post(); 

$post_id = get_the_ID();

$tablet_image = get_field('tablet_image', $post_id);
$mobile_image = get_field('mobile_image', $post_id);
$author_profile_pic = get_field('author_image', $post_id);
$author_sub_title = get_field('author_sub_title', $post_id);
$author_title = get_field('author_title', $post_id);

?>

      <div class="gnc-artclestpsection">
         <div class="gnc-articlescontainer">
			<div class="gnc-mainbreadcrumbcustom">
				<?php
					//echo do_shortcode('[elementor-template id="1094"]');
               echo do_shortcode('[elementor-template id="'.GNC_BREADCRUMB_SECTION_ID.'"]');
               
				?>
			</div>
            <div class="gnc-artclesbannerpanel">
               <div class="artctextblock">
                  <p class="artcldatetext"><?php echo get_the_date(); ?></p>
                  <h1 class="header"><?php the_title(); ?></h1>
                  <div class="gnc-authorpanel">
                     <div class="gnc-authorblock">
					 <?php if (isset($author_profile_pic) && $author_profile_pic!=''){ ?>
                        <div class="authorimg">

                           <img src="<?php echo $author_profile_pic['url'];?>" alt="img" class="atrimg">
                        </div>
					 <?php } ?>
                        <div class="author-name">
							<?php if (isset($author_title) && $author_title!=''){ ?>
							<span class="author-header"><?php echo $author_title; ?></span>
							<?php } ?>
							<?php if (isset($author_sub_title) && $author_sub_title!=''){ ?>
							<span class="author-subheader"><?php echo $author_sub_title; ?></span>
							<?php } ?>
                        </div>
                     </div>
                  </div>
               </div>
			   
               <div class="catgimgblock">
                  <div class="pd-image-picture">
                        <?php if(has_post_thumbnail()){ ?>
                           <img class="heroslide-tile-img" alt="image" src="<?php echo get_the_post_thumbnail_url(); ?>">
                        <?php } ?> 
                     </div>
               </div>

			   
            </div>
            <div class="gnc-artclscontentsection">
               <div class="gnc-artclwrapper">
                  
				  
				  
				  <?php  the_content(); ?>

				  
				  <?php if(get_field('references', $post_id)):?>
                  <div class="gnc-refernceblk">
                     <div class="gnc-reblk-content">
                        <span class="gncr-text">
                        References
                        </span>
                        <ul>
							<?php echo get_field('references', $post_id); ?>
                           
                        </ul>
                     </div>
                  </div>
				<?php endif;?>
                  <div class="gnc-recipeslst">
                     <div class="gnc-repdctslist">
						<?php $category = get_the_category();
							if($category){
								foreach($category as $firstCategory){
						?>
									<span><a href="<?php echo get_category_link($firstCategory->cat_ID ); ?>" target="_blank"><?php echo $firstCategory->name;?></a></span>
						<?php 
								}
							}
						?>
						<?php

							/* $posttags = get_the_tags($post_id);
							
							if ($posttags) {
							  foreach($posttags as $tag) {
								?>
									<span><a href="<?php echo get_tag_link( $tag->term_id ); ?>" target="_blank"><?php echo $tag->name; ?></a></span>
								<?php
							  }
							}  */
							?>
                     </div>
                  </div>
                     
                     

                  <div class="article-social-share-section social-share-section">
                  <?php echo do_shortcode('[elementor-template id="'.GNC_SOCIAL_SHARE_SECTION_ID.'"]'); ?>
                  </div>
				
                  </div>
               </div>
            </div>
         </div>
      </div>
<?php endwhile; ?>
<?php 
endif; 

wp_reset_postdata(); ?>

<?php echo do_shortcode('[articles-detail-related-articles-shortcode-section]'); ?>
<?php get_footer(); ?>
