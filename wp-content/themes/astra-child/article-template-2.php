<?php
/* 
* Template Name: Articles Template 2
* Template Post Type: Post
*/
get_header();
?>
<?php $post_id = get_the_ID(); ?>

      <div class="gnc-artclestpsection">
         <div class="gnc-articlescontainer">
			<div class="gnc-mainbreadcrumbcustom">
				<?php
					//echo do_shortcode('[elementor-template id="1094"]');
               echo do_shortcode('[elementor-template id="'.GNC_BREADCRUMB_SECTION_ID.'"]');
				?>
			</div>
			
            <div class="gnc-artclesbannerpanel">
               <div class="artctextblock">
                  <p class="artcldatetext"><?php echo get_the_date(); ?></p>
                  <h1 class="header"><?php the_title(); ?></h1>
               </div>
               <div class="catgimgblock">
                     <div class="pd-image-picture">
                        <?php if(has_post_thumbnail()){ ?>

                        <?php if(has_post_thumbnail()){ ?>
                              <img class="heroslide-tile-img" alt="image" src="<?php echo get_the_post_thumbnail_url(); ?>">
                              <?php } ?>

                        <?php } ?>
                  </div>
               </div>

            </div>
            <div class="gnc-artclscontentsection">
               <div class="gnc-artclwrapper">
                  <?php  the_content(); ?>
               </div>
            </div>

            <!-- Prduct Layout start -->

            <div class="gnc-artclscontentsection">
               <div class="gnc-productltpanel">
				<?php
					if(have_rows('product_type_images_article_2', $post_id)){
						$product_post_title = '';
						$product_post_image_url = '';
						$product_button_title='';
						$product_button_url = '';
						$product_post_title='';
						$product_post_content = '';
						$product_post_image_url='';
						$a=1;
						while(have_rows('product_type_images_article_2', $post_id)){
							the_row();
							
							$product_post_title = get_sub_field('title', $post_id);
							$product_post_image = get_sub_field('image', $post_id);
							if(isset($product_post_image['url']) && $product_post_image['url']!=''){
								$product_post_image_url=$product_post_image['url'];
							}
							
							$product_button_title=get_sub_field('button_title',$post_id);
							$product_button_url = get_sub_field('button_url', $post_id);
							$product_post_content = get_sub_field('content', $post_id);
							if($a%2 == 1){ 
				?>
			   
							  <div class="gnc-productltwrapper">
								 <div class="product-img">
								 <?php if(isset($product_post_image_url) && $product_post_image_url != ''){ ?>
									<img data-src="<?php echo $product_post_image_url; ?>" alt="image" src="<?php echo $product_post_image_url; ?>">
								 <?php } ?>
								 </div>
								 <div class="product-container">
									<div class="product-content">
										<?php if (isset($product_post_title) && $product_post_title!=''){ ?>
									   <h4><?php echo $product_post_title; ?></h4>
										<?php } ?>
										<?php if(isset($product_post_content) && $product_post_content != ''){ ?>
									   <p><?php echo $product_post_content; ?></p>
										<?php } ?>
									
										<?php if (isset($product_button_title) && $product_button_title!=''){ ?>
									   <div class="shopbtnblock">
										  <a href="<?php echo $product_button_url; ?>" class="shopbtn" target="_blank"><?php echo $product_button_title; ?></a>
									   </div>
									   <?php } ?>
									</div>
								 </div>
							  </div>
				<?php
							}else{
				?>
							<div class="gnc-productltwrapper gnc-productrlwrapper">
							 <div class="product-container">
								<div class="product-content">
								   <?php if (isset($product_post_title) && $product_post_title!=''){ ?>
								   <h4><?php echo $product_post_title; ?></h4>
									<?php } ?>
								   <?php if(isset($product_post_content) && $product_post_content != ''){ ?>
								   <p><?php echo $product_post_content; ?></p>
									<?php } ?>
								   
									<?php if (isset($product_button_title) && $product_button_title!=''){ ?>
									   <div class="shopbtnblock">
										  <a href="<?php echo $product_button_url; ?>" class="shopbtn" target="_blank"><?php echo $product_button_title; ?></a>
									   </div>
									   <?php } ?>
								</div>
							 </div>
							 <div class="product-img">
								 <?php if(isset($product_post_image_url) && $product_post_image_url != ''){ ?>
									<img data-src="<?php echo $product_post_image_url; ?>" alt="image" src="<?php echo $product_post_image_url; ?>">
								 <?php } ?>
								 </div>
						  </div>
								
				<?php			
							}
						$a++;
						}
					}
				?>
               </div>
            </div>

          <!-- Prduct Layout End -->
					
			<div class="recipe-social-share-section social-share-section">
			<?php echo do_shortcode('[elementor-template id="'.GNC_SOCIAL_SHARE_SECTION_ID.'"]'); ?>
			</div>


        
         </div>
      </div>

<?php echo do_shortcode('[articles-detail-related-articles-shortcode-section]'); ?>
<?php get_footer(); ?>
