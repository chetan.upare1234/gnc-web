$(function() {
      var $carousel = $(".categoriescarousel");
      var $slider;
      $carousel.slick({
      speed: 300,
      arrows: true,
      infinite: false,
      slidesToShow: 5,
      centerPadding: '50px',
      slidesToScroll: 1,
      prevArrow: '<div class="slick-prev"><</div>',
      nextArrow: '<div class="slick-next">></div>',
      responsive: [
            {
                breakpoint: 1460,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 1230,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false,
                }
            },
            {
                breakpoint: 766,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,

                }
            }
        ]
      }).on('afterChange', (e, slick, slide) => {
      $slider.slider('value', slide);
      });

      $slider = $(".categorie-rangeslider").slider({
      min: 0,
      max: 5,
      slide: function(event, ui) {
      var slick = $carousel.slick("getSlick");
      goTo = ui.value * (slick.slideCount - 1) / 5;
      $carousel.slick("goTo", goTo);
      }
      });
      });