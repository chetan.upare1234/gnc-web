$(document).ready(function(){
		
		// var delay = 100; setTimeout(function() { 
		// $('.elementor-tab-title').removeClass('elementor-active');
		// $('.elementor-tab-content').css('display', 'none'); }, delay); 
		
		
        $('.featurearticlecarousel').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          infinite: true,
          variableWidth: true,
          dots: false,
          swipe: true,
          autoplay: false,
          prevArrow: $('.prev'),
          nextArrow: $('.next'),
          responsive: [

              {
                  breakpoint:992,
                  settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      autoplaySpeed: 4000,
                      swipe: true,
                      arrows: false,
                      dots: true,
                  }
              }
          ]
        });

  var highestBox = 0;
      $('.facard-title h4').each(function(){  
              if($(this).height() > highestBox){  
              highestBox = $(this).height();  
      }
  });    
  $('.facard-title h4').height(highestBox);
 });
 