var pagestatus = false;
jQuery('.tagspost').on('click', function() {
      var termID = jQuery(this).attr("data-id");
      var catID = jQuery("#current_cat_id").val();
     jQuery('#tagepage').val(termID);
      jQuery.ajax({
     type: 'POST',
     url: '/wp-admin/admin-ajax.php',
     dataType: 'json',
     data: {
       action: 'get_post_by_tag',
       tagid: termID,
       catid: catID,
     },
     success: function (res) {
       jQuery('.tagpost-list').html(res.html);
       jQuery('#postfound').text(res.totalcnt);
       jQuery('#tagpost-load-more').show();
       jQuery('#tagpostbarsec').show();
 
       var perpagepost = res.postperpage;
       var postcount = res.postcount;
       if(res.totalcnt < 9){
           jQuery('#tagpost-load-more').hide();
           jQuery('#tagpostbarsec').hide();
       }
       jQuery('#load-post-count').text(postcount);
       jQuery('#tagpostprogressor').val(perpagepost);
       jQuery('#tagpostprogressor').prop('max',res.totalcnt);
     }
   });
 
 });
 let currentPage = 1;
 // load more tag post
 jQuery('#tagpost-load-more').on('click', function() {
   var termID = jQuery('.tagspost').attr("data-id");
   if(pagestatus == true){
     currentPage = 1;
     termID =  jQuery('#tagepage').val();
     pagestatus = false;
   }
   var catID = jQuery("#current_cat_id").val();
   currentPage++;
   jQuery.ajax({
     type: 'POST',
     url: '/wp-admin/admin-ajax.php',
     dataType: 'json',
     data: {
       action: 'tagpost_load_more',
       tagid: termID,
       catid: catID,
       paged: currentPage,
     },
     success: function (res) {
       var perpagepost = parseInt(jQuery('#load-post-count').text());
       var postcount = res.postcount;
       var sum = postcount + perpagepost;
       if(currentPage >= res.max) {
         jQuery('#tagpost-load-more').hide();
         jQuery('#tagpostbarsec').hide();
       }
       jQuery('.tagpost-list').append(res.html);
       jQuery('#load-post-count').text(sum);
       jQuery('#tagpostprogressor').val(sum);
     }
   });
 });

 