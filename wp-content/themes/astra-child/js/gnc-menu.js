/*
*  GNC MT Menu JS
*/
var droCatererMainMenu = $('.main-mtnavigation ul.menu');
$(droCatererMainMenu).droSlidingMenu();
$(document).ready(function() {
$("#closemtbtn").click(function(e) {
    $(".gnc-mt-wrapper").removeClass("active");
    $("#gnctoggle-mtmenu").removeClass("open");
    $(".mtmenu-item-back").removeClass("active");
});

$("#gnctoggle-mtmenu").click(function(e) {
    $(".closemt-btn").show();
});

});

 //Sticky GNC MT Menu
 
 $(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 5) {
        $("body").addClass("gnc-mt-sticky");
    } else {
        $("body").removeClass("gnc-mt-sticky");
    }
});
