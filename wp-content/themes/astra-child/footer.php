<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>
<?php astra_content_bottom(); ?>
	</div> <!-- ast-container -->
	</div><!-- #content -->
<?php 
	astra_content_after();
		
	astra_footer_before();
		
	astra_footer();
		
	astra_footer_after(); 
?>
	</div><!-- #page -->
<?php 
	astra_body_bottom();    
	wp_footer(); 
?>
	
<script type="text/javascript">
	function clearRecentSearch() {
		jQuery('div#recent_search').empty();
		jQuery.ajax({
			type: "post",
			dataType: "json",
			url: my_ajax_object.ajax_url,
			data: {
				action:'clearrecensearch'
			},
			/*success: function(msg){
				jQuery("#recent_search").html("");
			}*/
		});
	}
</script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri() . '/js/home-featured-article.js'; ?>"></script>
	</body>
</html>
