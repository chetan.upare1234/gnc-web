<?php
/* 
* Template Name: Articles Template 3
* Template Post Type: Post
*/
bhvj

get_header();
?>

<?php $post_id = get_the_ID(); 
     $body_title= get_field('body_title',$post_id);
     $button_title = get_field('button_title', $post_id);
     $button_url= get_field('button_url',$post_id);
     $video_url = get_field('video_url', $post_id);
     

?>

      <div class="gnc-artclestpsection">
         <div class="gnc-articlescontainer">
		 <div class="gnc-mainbreadcrumbcustom">
				<?php
					 echo do_shortcode('[elementor-template id="'.GNC_BREADCRUMB_SECTION_ID.'"]');
				?>
			</div>
            <div class="gnc-artclesbannerpanel">
               <div class="artctextblock">
                  <p class="artcldatetext"><?php echo get_the_date(); ?></p>
                  <h1 class="header"><?php the_title(); ?></h1>
               </div>
                     <div class="catgimgblock">
                        <div class="pd-image-picture">
                              <?php if(has_post_thumbnail()){ ?>
                                 <img class="heroslide-tile-img" alt="image" src="<?php echo get_the_post_thumbnail_url(); ?>">
                              <?php } ?> 
                           </div>
                      </div>

            </div>

            <div class="gnc-artclscontentsection">
               <div class="gnc-artclwrapper">
                  
                  <h4><?php echo $body_title; ?></h4>
                  <p>
                      <?php  the_content(); ?>
                  </p>

                  <div class="shopunbkbtn">
                     <a href="<?php echo get_field('button_url', $post_id); ?>" class="shopbtn" target="_blank"><?php echo  $button_title; ?></a>
                  </div>
               </div>
            </div>

            <div class="gnc-artclscontentsection">
               <div class="gnc-videopanel">


               <?php

					if(have_rows('video_content_repeater_article_template_3', $post_id)){
						$title = '';
						$content = '';
						$video_url = '';
						$a=1;
						while(have_rows('video_content_repeater_article_template_3', $post_id)){
							the_row();
							
							$title = get_sub_field('title', $post_id);
						    $content = get_sub_field('content', $post_id);
							$video_url = get_sub_field('video_url', $post_id);
							
							if($a%2 == 1){ 
				?>
			   

                  <div class="gnc-videowrapper">
                     <div class="video-container">
                        <div class="video-content">
                           <h4><?php echo $title; ?></h4>
                            <p><?php echo $content; ?></p>
                        </div>
                     </div>
                     <div class="video-playblock">
                        <iframe class="videoiframe" width="" height="405" border="0" src="<?php echo $video_url; ?>"></iframe>
                     </div>
                  </div>
				  
				  <?php
							}else{
				?>
                  <div class="gnc-videowrapper gnc-videorl">
                     <div class="video-playblock">
                        <iframe class="videoiframe" width="" height="405" border="0" src="<?php echo $video_url; ?>"></iframe>
                     </div>
                     <div class="video-container">
                        <div class="video-content">
                           <h4><?php echo $title; ?></h4>
                           <p><?php echo $content; ?></p>
                        </div>
                     </div>
                  </div>    
                  <?php			
							}
						$a++;
						}
					}
				?>
                 
                
               </div>
            </div>

               <div class="recipe-social-share-section social-share-section">
               <?php echo do_shortcode('[elementor-template id="'.GNC_SOCIAL_SHARE_SECTION_ID.'"]'); ?>
               </div>

         </div>
      </div>

<?php echo do_shortcode('[articles-detail-related-articles-shortcode-section]'); ?>
<?php get_footer(); ?>