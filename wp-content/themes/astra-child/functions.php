<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );
         
if ( !function_exists( 'child_theme_configurator_css' ) ):
    function child_theme_configurator_css() {
        wp_enqueue_style( 'chld_thm_cfg_child', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', array( 'astra-theme-css' ) );
    }
endif;

add_action( 'wp_enqueue_scripts', 'child_theme_configurator_css', 10 );

require_once(get_stylesheet_directory(). '/includes/home-page-settings.php');
require_once(get_stylesheet_directory(). '/includes/shortcodes.php');
require_once(get_stylesheet_directory(). '/includes/recipe-details-shortcode.php');
require_once(get_stylesheet_directory(). '/includes/clear-search-history.php');
require_once(get_stylesheet_directory(). '/includes/tag-filter-shortcode.php');
require_once(get_stylesheet_directory(). '/includes/gnc-mt-menu-walker.php');
require_once(get_stylesheet_directory(). '/includes/gnc-mt-menu-shortcode.php');
require_once(get_stylesheet_directory(). '/includes/hooks.php');


/*  Adding Theme Scripts & CSS  */

function add_theme_scripts() {

    /* add your css your */
    wp_enqueue_style( 'custom-hero', get_stylesheet_directory_uri().'/css/gnc-hero.css' );
    wp_enqueue_style( 'custom-slick', get_stylesheet_directory_uri().'/css/slick.css' );
    wp_enqueue_style('custom-slider',get_stylesheet_directory_uri().'/css/category-slider-homepage.css');
    wp_enqueue_style( 'custom-tag', get_stylesheet_directory_uri().'/css/tag-slider-homepage.css' );
    wp_enqueue_style( 'custom-recipe', get_stylesheet_directory_uri().'/css/recipe-detail.css' );
    wp_enqueue_style( 'custom-menu', get_stylesheet_directory_uri().'/css/mobile-menu.css' );
    wp_enqueue_style( 'fa', get_stylesheet_directory_uri().'/css/font-awesome.css' );

    /*For  Designers Custom Css */

    wp_enqueue_style( 'gnc-custom', get_stylesheet_directory_uri().'/css/gnc-custom.css' );

    

    /*add your js here */
    
    wp_enqueue_script( 'jqueryui', get_stylesheet_directory_uri() . '/js/jquery-ui.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'perfect-scroll', get_stylesheet_directory_uri() . '/js/perfect-scrollbar.jquery.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'perfect-scroll-tag', get_stylesheet_directory_uri() . '/js/tag-slider.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'perfect-scroll-cat', get_stylesheet_directory_uri() . '/js/cat-slider.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'tag-filter', get_stylesheet_directory_uri() . '/js/tag-filter.js', array ( 'jquery' ), 1.1, true);
    wp_enqueue_script( 'gnc-custom', get_stylesheet_directory_uri() . '/js/gnc-custom.js', array ( 'jquery' ), 1.1, true);
    
    wp_enqueue_script( 'gnc-custom-menu', get_stylesheet_directory_uri() . '/js/dro-sliding-menu.js', array ( 'jquery' ), 1.1, true);  
    wp_enqueue_script( 'gnc-custom', get_stylesheet_directory_uri() . '/js/gnc-menu.js', array ( 'jquery' ), 1.1, true); 
   // wp_enqueue_script( 'home-featured-article', get_stylesheet_directory_uri() . '/js/home-featured-article.js', array ( 'jquery' ), 1.1, true); 
}

add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );