<?php

define('DE_CIX_HOME_PAGE_PAGE_ID',8);

add_action( 'cmb2_admin_init', 'de_cix_contact_us_top_section_block_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_admin_init' or 'cmb2_init' hook.
 */
 
function de_cix_contact_us_top_section_block_metabox() {
    /**
     * Sample metabox to demonstrate each field type included
     */
    $cmb = new_cmb2_box( array(
        'id'            => 'de_cix_contact_us_top_section',
        'title'         => esc_html__( 'Add manual Articles', 'cmb2' ),
        'object_types'  => array( 'page' ), // Post type
        'show_on'      => array( 'key' => 'id', 'value' => array(DE_CIX_HOME_PAGE_PAGE_ID) ),

        // 'show_on_cb' => 'yourprefix_show_if_front_page', // function should return a bool value
        // 'context'    => 'normal',
        // 'priority'   => 'high',
        // 'show_names' => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // true to keep the metabox closed by default
        // 'classes'    => 'extra-class', // Extra cmb2-wrap classes
        // 'classes_cb' => 'yourprefix_add_some_classes', // Add classes through a callback.

        /*
         * The following parameter is any additional arguments passed as $callback_args
         * to add_meta_box, if/when applicable.
         *
         * CMB2 does not use these arguments in the add_meta_box callback, however, these args
         * are parsed for certain special properties, like determining Gutenberg/block-editor
         * compatibility.
         *
         * Examples:
         * Testing
         * 
         *
         * - Make sure default editor is used as metabox is not compatible with block editor
         *      [ '__block_editor_compatible_meta_box' => false/true ]
         *
         * - Or declare this box exists for backwards compatibility
         *      [ '__back_compat_meta_box' => false ]
         *
         * More: https://wordpress.org/gutenberg/handbook/extensibility/meta-box/
         */
        // 'mb_callback_args' => array( '__block_editor_compatible_meta_box' => false ),
    ) );
    
        $contact_us_top_section_card_block_repeater = $cmb->add_field( 
			array(
				'id'          => 'manual_featured_article_repeater',
				'type'        => 'group',
				'repeatable'  => true,
				'options'     => array(
					'group_title'   => 'Add Block Repeater {#}',
					'add_button'    => 'Add Another Post',
					'remove_button' => 'Remove Post',
					'closed'        => true,  // Repeater fields closed by default - neat & compact.
					'sortable'      => true,  // Allow changing the order of repeated groups.
				),
			) 
		);
            $cmb->add_group_field( $contact_us_top_section_card_block_repeater, array(
                'name' => 'Article Title',
                'desc' => 'Article Title',
                'id'   => 'manual_featured_article_title',
                'type' => 'text',
            ) );
            
			$cmb->add_group_field( $contact_us_top_section_card_block_repeater, array(
                'name'    => 'Upload Image',
                'desc'    => 'Upload Image',
                'id'      => 'manual_featured_article_upload_image',
                'type'    => 'file',
                //'after_row' => '<h3>Add Content For Right Aligned Box</h3>',
                //'closed'        => false, 
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' => 'Add Background' // Change upload button text. Default: "Add or Upload File"
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    //'type' => 'application/pdf', // Make library only display PDFs.
                    // Or only allow gif, jpg, or png images
                    // 'type' => array(
                    //     'image/gif',
                    //     'image/jpeg',
                    //     'image/png',
                    // ),
                ),
                'preview_size' => 'small', // Image size to use when previewing in the admin.
            ) );

              

            $cmb->add_group_field( $contact_us_top_section_card_block_repeater, array(
                'name' => 'Button URL',
                'desc' => 'Button UR',
                'id'   => 'manual_featured_article_url',
                'type' => 'text',
            ) );    

}

