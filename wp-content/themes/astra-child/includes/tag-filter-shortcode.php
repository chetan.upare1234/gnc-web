<?php
//custom code
add_shortcode('tagfilter','add_tag_filter');
function add_tag_filter(){
  $current_cat = get_cat_ID( single_cat_title("",false) );
  //$current_cat =  10;
  $args=array(
    'cat'  =>  $current_cat,
    'posts_per_page'  =>  -1,
    'post_type' => 'post',
    'post_status' => 'publish'
  );
  $custom_query = new WP_Query($args);
  if ($custom_query->have_posts()) :
      while ($custom_query->have_posts()) : $custom_query->the_post();
          $posttags = get_the_tags();
          if ($posttags) {
              foreach($posttags as $tag) {
                  $all_tags[] = $tag->term_id;
              }
          }
      endwhile;
  endif; 

  if(!empty($all_tags)){
      $tags_arr = array_unique($all_tags);
      ?>
      <input type="hidden" name="current_cat_id" id="current_cat_id" value="<?php echo $current_cat; ?>">
      <div class="search-tgspanel">
      <div class="search-tagsslide">
         <div class="search-tagsslidewrapper">
            <span>
            <a href="javascript:void(0);" class="tagspost active" data-id=<?php echo json_encode(array_values($tags_arr)); ?>>View All</a>
            </span>
            <?php
              foreach($tags_arr as $tagnlist) {
              $term_name = get_term( $tagnlist )->name;
              echo '<span class="tagspost" data-id="'.$tagnlist.'"><a href="javascript:void(0);">'.$term_name.'</a></span>';
              }
            ?>
         </div>
      </div>
   </div>
  <h2 class="gridart-heading">All <?php echo strtoupper(get_cat_name( $current_cat )); ?> ARTICLES</h2>
      <?php
  } 
  $articles = new WP_Query([
  'post_type' => 'post',
  'posts_per_page' => 10,
  'orderby' => 'date',
  'order' => 'DESC',
  'paged' => 1,
  'post_status' => 'publish',
  'cat' => $current_cat
  /*'tax_query' => array(
        array(
            'taxonomy' => 'post_tag',
            'field' => 'term_id',
            'operator' => 'IN',
            'terms'    => array_values($tags_arr),
        ),
    )*/
]);
?>

<?php if($articles->have_posts()): ?>
  <input type="hidden" id="tagepage" name="tagepage" value="">
  <div class="tagpost-list article-list-grid">
    <?php 
      while ($articles->have_posts()): $articles->the_post();
       ?>
          <?php get_template_part( 'template-parts/blog/tagpost-layout', get_post_format() ); ?>
       <?php
      endwhile;
    ?>
  </div>
<?php endif; ?>
<?php wp_reset_postdata(); ?>

<div class="btn__wrapper">
  <a href="javascript:void(0);" class="btn btn__primary" id="tagpost-load-more">Load more</a>
</div>

<div id="tagpostbarsec">
<progress id='tagpostprogressor' value="<?php echo $articles->post_count;?>" max='<?php echo $articles->found_posts; ?>'></progress>
<span id='load-post-count'><?php echo $articles->post_count; ?></span> Of 
<span id='postfound'><?php echo  $articles->found_posts; ?></span>
</div>

  <?php
}


add_action('wp_ajax_get_post_by_tag', 'get_post_by_tag');
add_action('wp_ajax_nopriv_get_post_by_tag', 'get_post_by_tag');
function get_post_by_tag() {
   $tag_ids = json_decode($_POST['tagid']);
    if(is_array($tag_ids)){
      $tax_query = array(
        array(
            'taxonomy' => 'category',
            'field' => 'id',
            'terms'    => $_POST['catid'],
        )
      );
    } 
    // For single tags
    else {
       $tax_query = array(
        array(
            'taxonomy' => 'post_tag',
            'field' => 'term_id',
            'terms' => $_POST['tagid']
            )
        );
    } 
  $ajaxposts = new WP_Query([
    'post_type' => 'post',
    'posts_per_page' => 10,
    'orderby' => 'date',
    'order' => 'DESC',
    'paged' => 1,
    'tax_query' => $tax_query,
    'post_status' => 'publish'
  ]);

  $response = '';
   $max_pages = $ajaxposts->max_num_pages;
   $found_posts = $ajaxposts->found_posts;
   $post_found = $ajaxposts->post_count;
  if($ajaxposts->have_posts()) {
     ob_start();
    while($ajaxposts->have_posts()) : $ajaxposts->the_post();
      $response .= get_template_part( 'template-parts/blog/tagpost-layout', get_post_format() );
    endwhile;
     $output = ob_get_contents();
    ob_end_clean();
  } else {
    $response = '';
  }

  $result = [
  'max' => $max_pages,
  'totalcnt' => $found_posts,
  'postcount' => $post_found,
  'postperpage' => 10,
  'html' => $output,
  ];
  echo json_encode($result);
  exit;
}
// load more ajax
function tagpost_load_more() {
   $tag_ids = json_decode($_POST['tagid']);
    if(is_array($tag_ids)){
      $tax_query = array(
        array(
            'taxonomy' => 'category',
            'field' => 'id',
            'terms' => $_POST['catid'],
        )
      );
    } 
    // For single tags
    else {
       $tax_query = array(
        array(
            'taxonomy' => 'post_tag',
            'field' => 'term_id',
            'terms' => $_POST['tagid']
            )
        );
    } 
  $ajaxposts = new WP_Query([
    'post_type' => 'post',
    'posts_per_page' => 10,
    'orderby' => 'date',
    'order' => 'DESC',
    'paged' => $_POST['paged'],
    'tax_query' => $tax_query,
    'post_status' => 'publish'
  ]);

  $response = '';
 $max_pages = $ajaxposts->max_num_pages;
   $found_posts = $ajaxposts->found_posts;
   $post_found = $ajaxposts->post_count;

  if($ajaxposts->have_posts()) {
    ob_start();
    while($ajaxposts->have_posts()) : $ajaxposts->the_post();
      $response .= get_template_part( 'template-parts/blog/tagpost-layout', get_post_format() );
    endwhile;
    $output = ob_get_contents();
    ob_end_clean();
  } else {
    $response = '';
  }

  $result = [
  'max' => $max_pages,
  'totalcnt' => $found_posts,
  'postcount' => $post_found,
  'postperpage' => 10,
  'html' => $output,
  ];
  echo json_encode($result);
  exit;
}
add_action('wp_ajax_tagpost_load_more', 'tagpost_load_more');
add_action('wp_ajax_nopriv_tagpost_load_more', 'tagpost_load_more');