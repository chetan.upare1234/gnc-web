<?php
/*
      GNC Mobile & Tablet Menu Walker Class Extending Default WP Walker
*/

// Registering new Navigation Menu For GNC MT Menu 

if (!function_exists('gnc_register_nav_menu')) {

    function gnc_register_nav_menu()
    {
        register_nav_menus(array(
            'slidein_menu' => __('Mobile & Tab Menu', 'text_domain')
        ));
    }
    add_action('after_setup_theme', 'gnc_register_nav_menu', 0);
}

add_action('wp_footer', 'add_theme_scripts');

// Removing Default Walker Classes From GNC MT Menu

function custom_gnc_nav_menu($var)
{
    return is_array($var) ? array_intersect(
        $var,
        array(
            //List of allowed menu classes
            'current_page_item',
            'current_page_parent',
            'current_page_ancestor',
            'first',
            'last',
            'vertical',
            'horizontal',
            'mtmenu-item-back',
            'active',
            'menu-item'
        )
    ) : '';
}
add_filter('nav_menu_css_class', 'custom_gnc_nav_menu');
add_filter('nav_menu_item_id', 'custom_gnc_nav_menu');
add_filter('page_css_class', 'custom_gnc_nav_menu');

// Extending Walker Nav Menu For GNC MT Menu

class GNC_Walker_Nav_Menu extends Walker_Nav_Menu
{

    public function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output)
    {
        $id_field = $this->db_fields['id'];

        /*checking if parent has child */
        $id_field = $this->db_fields['id'];
        if (is_object($args[0])) {
            $args[0]->has_children = !empty($children_elements[$element->$id_field]);
        }
        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }


    public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $indent = ($depth) ? str_repeat("\t", $depth) : '';
        $classes = empty($item->classes) ? array() : (array) $item->classes;

        $gnc_next_arrow = ' <span class="fal fa-chevron-right forward"></span>';
        $gnc_js_void    = '<a href="javascript:void(0)"></a>';

        $item_output = $args->before;

        if ($args->has_children) {
            $item->classes[] = 'mtmenu-item-back'; //applying item back which has children
            $item_output .= $args->link_before . $gnc_js_void . $args->link_after;
        }
        parent::start_el($output, $item, $depth, $args);

        $item_output .= '</a>';
        //$item_output .= $args->after;
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }
    public function end_el(&$output, $item, $depth = 0, $args = array())
    {
        $output .= "</li>\n";
    }
}
