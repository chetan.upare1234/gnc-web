<?php
function change_role_name() {
    global $wp_roles;
    if ( ! isset( $wp_roles ) )
    $wp_roles = new WP_Roles();
    $wp_roles->roles['editor']['name'] = 'Reviewer';
    $wp_roles->role_names['editor'] = 'Reviewer';           
}
add_action('init', 'change_role_name');

add_action( 'admin_init', 'my_remove_menu_pages' );
function my_remove_menu_pages() {
    if ( current_user_can( 'author' ) ) {
        remove_menu_page( 'edit.php?post_type=article' );
        remove_menu_page( 'edit.php?post_type=ribbon_banner' );
        remove_menu_page( 'tools.php' );
        remove_menu_page( 'edit-comments.php' );
        remove_menu_page( 'edit.php?post_type=elementor_library' );
        remove_menu_page( 'edit.php?post_type=elementor-hf' );
        remove_menu_page( 'themes.php' );
    }
}




