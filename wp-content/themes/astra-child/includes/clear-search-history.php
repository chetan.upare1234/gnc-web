<?php
/**
 * Session For Clear Seacch result
 */


/*function my_enqueue() {
wp_enqueue_script( 'ajax-script', get_template_directory_uri() . '/js/my-ajax-script.js', array('jquery') );
wp_localize_script( 'ajax-script', 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
}
add_action( 'wp_enqueue_scripts', 'my_enqueue' );*/


add_action( 'wp_ajax_nopriv_clearrecensearch', 'clearrecensearch' );
add_action( 'wp_ajax_clearrecensearch', 'clearrecensearch' );

function clearrecensearch(){
    if(!session_id()) {
        session_start();
    }
    unset($_SESSION['recentsearch']);
    wp_die();
}

function ses_init() {
   if (!session_id())
   session_start();
}
add_action('init','ses_init');