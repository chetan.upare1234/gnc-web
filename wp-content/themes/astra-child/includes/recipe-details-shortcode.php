<?php 
// function that runs when shortcode is called
function recipe_detail_author_section_callback() { 
$data=array();
$id=get_the_ID();

$data['author_title'] = get_field( "author_title",$id);
$data['author_sub_title'] = get_field( "author_sub-title",$id);
$image_arr = get_field( "author_image",$id);

$data['author_image_url'] ='';
$data['author_image_arr']=array();

if(isset($image_arr) && is_array($image_arr)){

   $data['author_image_arr'] = $image_arr;

   if(isset($image_arr['url']) && $image_arr['url']!=''){

      $data['author_image_url'] = $image_arr['url'];

   }
}


?>
<div class="gnc-authorpanel">
         <div class="gnc-authorblock">
           
         <?php 
            if(isset($data['author_image_url']) && $data['author_image_url'] !=''){
          ?> 
         
         <div class="authorimg">
          
         <img src="<?php  echo $data['author_image_url'];?>" alt="img" class="atrimg">        
         <?php 
         }
         ?>  
            </div>
            <div class="author-name">
               <?php 
                if(isset($data['author_title']) && $data['author_title'] !=''){
               ?>
               <span class="author-header"><?php echo $data['author_title']; ?></span>
               <?php 
                   } 
               ?>
               <?php 
                if(isset($data['author_sub_title']) && $data['author_sub_title'] !=''){
               ?>
               <span class="author-subheader"><?php echo $data['author_sub_title']; ?></span><?php 
                   } 
               ?>
               
            </div>
         </div>
      </div>
<?php       
      
}
// register shortcode
add_shortcode('recipe-detail-author-section-shortcode', 'recipe_detail_author_section_callback');

function recipe_detail_category_list_callback(){
?>
 <!-- List section start -->
      <div class="gnc-recipeslst">
         <div class="gnc-repdctslist">
               <?php 

                $post_type = get_post_type(get_the_ID());   
                $taxonomies = get_object_taxonomies($post_type);   
                $taxonomy_names = wp_get_object_terms(get_the_ID(), $taxonomies,array('term_id','name'),array('parent'=>0)); 

                $arr_category_list=array();

                foreach ($taxonomy_names as $single_taxonomy) {
                    
                    if($single_taxonomy->term_id!=17){

                     $link= get_term_link($single_taxonomy->slug, 'post');
                     $category_link = get_category_link($single_taxonomy->term_id);
                     

                    $arr_category_list[]=array(
                        'term_id'=>$single_taxonomy->term_id,
                        'name'=>$single_taxonomy->name,
                        'url'=>$category_link 
                    );
                }
             }

               ?>
                <?php 
                foreach($arr_category_list as $single_category){


                ?>
               <a href="<?php echo esc_url( $single_category['url'] ); ?>"><span><?php echo $single_category['name']; ?></span></a>
                <?php 

                }
                ?>

               <?php 

               ?>
         </div>
      </div>

 <!-- List section End -->

<?php 
}
add_shortcode('recipe-detail-category-section-shortcode', 'recipe_detail_category_list_callback');



add_shortcode('recipe-detail-related-articles-shortcode-section', 'recipe_detail_related_articles_section_shortcode_callbcak');
function recipe_detail_related_articles_section_shortcode_callbcak(){



$args = array('post_type' => 'post',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field' => 'slug',
                'terms' =>'recipes',
            ),
        ),
     );

$relate_post_loop = new WP_Query($args);

$arr_related_article_list=array();

foreach ($relate_post_loop->posts as $single_post_loop) {
    
//echo "<pre>"; 
//print_r($single_post_loop);

$single_post_loop_image_url = wp_get_attachment_url(get_post_thumbnail_id($single_post_loop->ID, 'thumbnail' ));



   $arr_related_article_list[]=array(

       'post_id'=>$single_post_loop->ID,
       'post_title'=>$single_post_loop->post_title,
      // 'post_content'=>$single_post_loop->post_content,
       'image'=>$single_post_loop_image_url,
       'permalink'=>get_permalink( $single_post_loop->ID )
   );  
}

$data['related_articles']=$arr_related_article_list;


?>
   <link href="<?php echo get_template_directory_uri(); ?>/css/slick.css" rel="stylesheet">
      <link href="<?php echo get_template_directory_uri(); ?>/css/custom-homepage.css" rel="stylesheet">
   </head>
   <body>

       <div class="related-articles-continer">
         <div class="relatedarticles-wrapper">
            <div class="relatedinfo-wrap">
               <div class="subheader">
                     <h2>Related  Articles</h2>
               </div>
               <div class="faslickbtn">
                  <ul class="faslickbtnlist">
                     <li class="prev"></li>
                     <li class="next"></li>
                  </ul>
               </div>
            </div>
            <div class="relatedarticles-component">
               <div class="relatedarticlecarousel">
                  
                  <?php 
                   $a=1;
                  foreach ($arr_related_article_list as $single_article){

                    
                   ?>
                    <span>
                     <div class="related-articles-inner">
                        <div class="famedia-wrap">
                           <div class="raediacontent">
                              <picture class="pd-image-picture">
                                 <img class="related-article-tile-img" data-src="<?php echo $single_article['image']; ?>" alt="Pre Workouts" src="<?php echo $single_article['image']; ?>">
                              </picture>
                           </div>
                           <i class="fa-brand-<?php if($a%2 == 1){echo 'patternone';}else{echo 'patterntwo';} ?> fa-pattern"></i>
                        </div>
                        <div class="cardinfo-wrap">
                           <div class="facard-text-wrap">
                              <div class="related-article-acard-title">
                                 <h4><?php echo $single_article['post_title']; ?> </h4>
                              </div>
                              <div class="card-btn"><a class="card-link" href="<?php echo $single_article['permalink']; ?>" target="" >Read Article</a></div>
                           </div>
                        </div>
                     </div>
                  </span>


                   <?php 
                    $a++;
                  }
                  ?>
               </div>
            </div>
         </div>
      </div>
              
<?php 
}

add_action('wp_footer', 'adding_custom_style_for_recipe_details');
function adding_custom_style_for_recipe_details(){

if ( is_singular( 'post' ) ) {

?>
   <script>
      $(document).ready(function(){

         
                $('.relatedarticlecarousel').slick({
                  slidesToShow: 2,
                  slidesToScroll: 1,
                  arrows: true,
                  infinite: false,
                  variableWidth: true,
                  dots: false,
                  swipe: true,
                  autoplay: true,
                  prevArrow: $('.prev'),
                  nextArrow: $('.next'),
                  responsive: [
        
                      {
                          breakpoint:992,
                          settings: {
                              slidesToShow: 2,
                              slidesToScroll: 1,
                              autoplaySpeed: 4000,
                              swipe: true,
                              arrows: false,
                              dots: true,
                          }
                      }
                  ]
                });
      
          var highestBox = 0;
              $('.related-article-acard-title h4').each(function(){  
                      if($(this).height() > highestBox){  
                      highestBox = $(this).height();  
              }
          });    
          $('.related-article-acard-title h4').height(highestBox);



         });

      
   </script>       
<?php 

   }
}