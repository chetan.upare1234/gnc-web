<?php
/*
      GNC Mobile & Tablet Menu Shortcode
      -- [gnc-mt-menu]
*/

add_shortcode('gnc-mt-menu', 'add_gnc_mt_menu_shortcode');

function add_gnc_mt_menu_shortcode()
{
?>

    <nav id="" class="main-mtnavigation" role="navigation">

        <ul class="menu nav-mtmenu">
            <div class="main-tglebtnclose">
                <div class="closemt-btn" id="closemtbtn">
                    <i class="fal fa-times"></i>
                </div>
            </div>
        </ul>
        <?php $defaults = array(
            'theme_location'  => 'slidein_menu',
            'container'       => 'ul',
            'menu_id'         => 'gnc-primary-mtmenu',
            'menu_class'      => 'menu nav-mtmenu',
            'walker'          => new GNC_Walker_Nav_Menu(),
            'items_wrap' => '<ul aria-expanded="false" id="%1$s" class="%2$s">%3$s</ul>'
        );

        wp_nav_menu($defaults);

        ?>


    </nav>
<?php

}