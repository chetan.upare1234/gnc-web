<?php 
add_shortcode('manual-entry-featured-article', 'custom_manal_entry_featured_article_callback');
function custom_manal_entry_featured_article_callback(){
$data['manual_featured_article_repeater'] = get_post_meta(DE_CIX_HOME_PAGE_PAGE_ID, 'manual_featured_article_repeater', true);

if( get_field('article') == 'ex' ) {
?>
<div class="featurearticles-container">
<div class="featurearticles-wrapper">
<div class="featureartinfo-wrap">
<div class="subheader">
<h2><?php echo get_field('featured_articles_lavel'); ?></h2>
</div>
<div class="faslickbtn">
<ul class="faslickbtnlist">
<li class="prev"></li>
<li class="next"></li>
</ul>
</div>
</div>
<div class="featurearticles-component">
<div class="featurearticlecarousel">

<?php
	$args = array( 'post_type' => 'post', 'posts_per_page' => 10 );
	$the_query = new WP_Query( $args ); 
	$a = 1;
	if ( $the_query->have_posts() ){ 
		while ( $the_query->have_posts() ){
			$the_query->the_post();
			if(get_field('is_featured') == 'True'){
			?>
			
			
			
		<span>
			<div class="featurearticles-inner">
				<div class="famedia-wrap">
				   <div class="famediacontent">
					  <picture class="pd-image-picture">
						 <img class="feature-tile-img" data-src="images/pre-workouts.jpg" alt="Pre Workouts" src="<?php echo get_the_post_thumbnail_url($the_query->ID); ?>">
					  </picture>
				   </div>
				   <i class="fa-brand-<?php if($a%2 == 1){echo 'patternone';}else{echo 'patterntwo';} ?> fa-pattern"></i>
				</div>
				<div class="cardinfo-wrap">
				   <div class="facard-text-wrap">
					  <div class="facard-title">
						 <h4><?php the_title(); ?></h4>
					  </div>
					  <div class="card-btn"><a class="card-link" href="<?php echo get_permalink(); ?>" target="" >Read Article</a></div>
				   </div>
				</div>
			</div>
		</span>
		
			<?php
			}
			$a++;
		}
	}
?>
	</div>
	</div>
 </div>
</div>
<?php
}
if( get_field('article') == 'ma' ) {
?>
<div class="featurearticles-container">
<div class="featurearticles-wrapper">
<div class="featureartinfo-wrap">
<div class="subheader">
<h2><?php echo get_field('featured_articles_lavel'); ?></h2>
</div>
<div class="faslickbtn">
<ul class="faslickbtnlist">
<li class="prev"></li>
<li class="next"></li>
</ul>
</div>
</div>
<div class="featurearticles-component">
<div class="featurearticlecarousel">
<?php
if($data['manual_featured_article_repeater']){
	$a = 1;
	foreach($data['manual_featured_article_repeater'] as $single_manual_featured_article){
	?>

	<span>
		<div class="featurearticles-inner">
			<div class="famedia-wrap">
			   <div class="famediacontent">
				  <picture class="pd-image-picture">
					 <img class="feature-tile-img" data-src="images/pre-workouts.jpg" alt="Pre Workouts" src="<?php echo $single_manual_featured_article['manual_featured_article_upload_image']; ?>">
				  </picture>
			   </div>
			   
				   <i class="fa-brand-<?php if($a%2 == 1){echo 'patternone';}else{echo 'patterntwo';} ?> fa-pattern"></i>
			</div>
			<div class="cardinfo-wrap">
			   <div class="facard-text-wrap">
				  <div class="facard-title">
					 <h4><?php echo $single_manual_featured_article['manual_featured_article_title']; ?></h4>
				  </div>
				  <div class="card-btn"><a class="card-link" href="<?php echo $single_manual_featured_article['manual_featured_article_url']; ?>" target="" >Read Article</a></div>
			   </div>
			</div>
		</div>
	</span>

	<?php 
	   $a++;
	   }
	}
}

?>
</div>
	</div>
 </div>
</div>

<?php
 
}


add_shortcode('articles-detail-related-articles-shortcode-section', 'article_detail_related_articles_section_shortcode_callbcak');
function article_detail_related_articles_section_shortcode_callbcak(){
	
	$post_id = get_the_ID();
	$categoryids = array();
	$category = get_the_category($post_id);
	foreach($category as $cat_id){
		array_push($categoryids, $cat_id->cat_ID);
	}

	$args = array('category' => $categoryids);
	$related_posts = get_posts( $args );
	if( $related_posts ){
?>

<div class="featurearticles-container">
<div class="featurearticles-wrapper">
<div class="featureartinfo-wrap">
<div class="subheader">
<h2>Related Articles</h2>

</div>
<div class="faslickbtn">
<ul class="faslickbtnlist">
<li class="prev"></li>
<li class="next"></li>
</ul>
</div>
</div>
<div class="featurearticles-component">
<div class="featurearticlecarousel">

<?php
	
		$a=1;
		foreach($related_posts as $myposts){
			$post_title = $myposts->post_title;
			$post_image_url = get_the_post_thumbnail_url($myposts->ID);
			$post_url = get_permalink($myposts->ID);
?>	
<span>
	<div class="featurearticles-inner">
		<div class="famedia-wrap">
		<?php if($post_image_url){?>
		   <div class="famediacontent">
			  <picture class="pd-image-picture">
				 <img class="feature-tile-img" data-src="images/pre-workouts.jpg" alt="Pre Workouts" src="<?php echo $post_image_url; ?>">
			  </picture>
		   </div>
		<?php } ?>
		   <i class="fa-brand-<?php if($a%2 == 1){echo 'patternone';}else{echo 'patterntwo';} ?> fa-pattern"></i>
		</div>
		<div class="cardinfo-wrap">
		   <div class="facard-text-wrap">
			  <div class="facard-title">
				 <h4><?php echo $post_title; ?></h4>
			  </div>
			  <div class="card-btn"><a class="card-link" href="<?php echo $post_url; ?>" target="" >Read Article</a></div>
		   </div>
		</div>
	</div>
</span>

<?php		
		$a++;
		}
		
	
?>
</div>
	</div>
 </div>
</div>
<?php
}
}

add_shortcode('article-social-share-shortcode-section', 'article_social_share_section_shortcode_callbcak');
function article_social_share_section_shortcode_callbcak(){
	$post_id = get_the_ID();
	$post_url = get_permalink($post_id);
	$post_title = get_the_title($post_id);
	
?>

<div class="gnc-printicon">
	<ul>
		<li>
			<a  href="mailto:?subject=<?php echo $post_title . " - " . get_bloginfo('name'); ?>&amp;body=<?php echo $post_url;?>"  target="_blank"> 
				<svg width="22" height="24" viewBox="0 0 22 24" fill="#1A1A1A" xmlns="http://www.w3.org/2000/svg">
				<path d="M19.25 7.50001H17.75C17.3375 7.50001 17 7.83751 17 8.25001C17 8.66251 17.3375 9.00001 17.75 9.00001H19.25C19.6625 9.00001 20 9.33751 20 9.75001V21.75C20 22.1631 19.6631 22.5 19.25 22.5H2.75C2.33703 22.5 2 22.1625 2 21.75V9.75001C2 9.33751 2.33703 9.00001 2.75 9.00001H4.25C4.6625 9.00001 5 8.66251 5 8.25001C5 7.83751 4.6625 7.50001 4.25 7.50001H2.75C1.50922 7.50001 0.5 8.50782 0.5 9.75001V21.75C0.5 22.9922 1.50922 24 2.75 24H19.25C20.4908 24 21.5 22.9908 21.5 21.75V9.75001C21.5 8.50782 20.4922 7.50001 19.25 7.50001ZM6.5 6.00001C6.69191 6.00001 6.88381 5.92679 7.03016 5.78026L10.25 2.56079V15.75C10.25 16.1646 10.5854 16.5009 11 16.5009C11.4146 16.5009 11.75 16.1625 11.75 15.75V2.56079L14.9698 5.78063C15.1156 5.92501 15.3078 6.00001 15.5 6.00001C15.9284 6.00001 16.25 5.64971 16.25 5.25001C16.25 5.0581 16.1768 4.8662 16.0303 4.71985L11.5303 0.219853C11.3844 0.0732281 11.1922 0 11 0C10.8078 0 10.6156 0.0732281 10.4703 0.219759L5.97031 4.71976C5.825 4.86563 5.75 5.05782 5.75 5.25001C5.75 5.64845 6.07344 6.00001 6.5 6.00001Z"/>
				</svg>
			</a>
		</li>
		<li>
			<a ref="javascript:void(0);" onclick="window.print();">
				<svg width="24" height="24" viewBox="0 0 24 24" fill="#1A1A1A" xmlns="http://www.w3.org/2000/svg">
				<path d="M19.5 15H4.5C3.67172 15 3 15.6717 3 16.5V22.5C3 23.3283 3.67172 24 4.5 24H19.5C20.3283 24 21 23.3283 21 22.5V16.5C21 15.6703 20.3297 15 19.5 15ZM19.5 22.5H4.5V16.5H19.5V22.5ZM21 9V3.31078C21 2.91281 20.8406 2.53125 20.5594 2.25L18.7486 0.439219C18.4266 0.157969 18.0891 0 17.6906 0H6C4.34297 0 3 1.34297 3 3V9C1.34531 9 0 10.3453 0 12V17.25C0 17.6625 0.335438 18 0.75 18C1.16456 18 1.5 17.6625 1.5 17.25V12C1.5 11.1717 2.17172 10.5 3 10.5H21C21.8283 10.5 22.5 11.1717 22.5 12V17.25C22.5 17.6646 22.8354 18 23.25 18C23.6646 18 24 17.6646 24 17.25V12C24 10.3453 22.6547 9 21 9ZM19.5 9H4.5V3C4.5 2.17172 5.17172 1.5 6 1.5H17.6906L19.5 3.31078V9ZM20.25 11.625C19.6289 11.625 19.125 12.1284 19.125 12.75C19.125 13.3711 19.6289 13.875 20.25 13.875C20.8711 13.875 21.375 13.3711 21.375 12.75C21.375 12.1266 20.8734 11.625 20.25 11.625Z"/>
				</svg>
			</a>
		</li>
	</ul>
</div>
						
<?php 
}
?>