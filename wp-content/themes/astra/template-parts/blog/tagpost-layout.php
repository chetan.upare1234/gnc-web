<article class="article-box">
  <div class="article-image">
    <picture class="pd-image-picture">
      <source media="(min-width:1024px)" srcset="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'medium' );  ?>">
      <source media="(min-width:521px)" srcset="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'medium' );  ?>">
      <img class="heroslide-tile-img" data-src="i<?php echo get_the_post_thumbnail_url( get_the_ID(), 'medium' );  ?>" alt="image" src="<?php echo get_the_post_thumbnail_url( get_the_ID(), 'medium' );  ?>">
    </picture>
  </div>
  <h3 class="card-title"><?php the_title(); ?> </h4>
    <p class="card-subtitle"><?php echo wp_trim_words( get_the_content(),20, '...'); ?></p>
    <div class="card-btn">
      <a class="card-link" href="<?php the_permalink(); ?>" target="">Read More</a>
    </div>
</article>